package polymorphism;

public class BookStore {
    public static void main(String[]args){
        Book[] b = new Book[5];
        b[0] = new Book("How i met your dragon", "Alec Morin");
        b[2] = new Book("The art of wasting your time", "Alex Mowin");
        b[1] = new ElectronicBook("What it means to give up", "Cela Nirom", 250);
        b[3] = new ElectronicBook("How to set up an apointment without parents manuel", "Ed", 301);
        b[4] = new ElectronicBook("I cant think of a clever name", "Kuai Liang", 05);

        for (int i = 0; i<b.length; i++){
            System.out.println(b[i]);
        }

        ElectronicBook bok = (ElectronicBook)b[1];
        System.out.println(bok.getNumberBytes());

        ElectronicBook bok2 = (ElectronicBook)b[0];
        System.out.println(bok.getNumberBytes());
    }
}